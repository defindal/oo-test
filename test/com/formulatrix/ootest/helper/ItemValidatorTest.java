/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.helper;

import com.formulatrix.ootest.entity.Item;
import com.formulatrix.ootest.exception.InvalidItemTypeException;
import com.formulatrix.ootest.helper.ItemValidator;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author deph
 */
public class ItemValidatorTest {
    
    private String validJSON = "{\"id\":2,\"name\":\"deph\"}";
    private String validXML = "<?xml version='1.0' encoding='UTF-8'?><rss></rss>";
    
    @Test
    public void validateJSONContent(){
        
        Assert.assertTrue(ItemValidator.isValidJSON(validJSON));
        Assert.assertFalse(ItemValidator.isValidJSON(validXML));
    }
 
    @Test
    public void validateXMLContent(){
        Assert.assertTrue(ItemValidator.isValidXML(validXML));
        Assert.assertFalse(ItemValidator.isValidXML(validJSON));
    }
    
    @Test
    public void validateTypeContent() throws InvalidItemTypeException{
        Assert.assertTrue(ItemValidator.validate(validJSON,1));
        Assert.assertFalse(ItemValidator.validate(validXML,1));
        Assert.assertTrue(ItemValidator.validate(validXML,2));
        Assert.assertFalse(ItemValidator.validate(validJSON,2));
    }
    
    
    
    
    
}
