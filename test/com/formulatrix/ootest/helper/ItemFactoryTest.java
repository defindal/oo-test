/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.helper;

import com.formulatrix.ootest.exception.InvalidItemTypeException;
import com.formulatrix.ootest.helper.ItemFactory;
import com.formulatrix.ootest.sample.TestData;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author deph
 */
public class ItemFactoryTest {
    
    
    
    
    @Test
    public void BuildValidJSON() throws ParseException{
        Assert.assertNotNull(
                ItemFactory.buildJSONObject(TestData.validJSON));
    }
    
    @Test(expected = Throwable.class )
    public void BuildInvalidJSON() throws ParseException{
        ItemFactory.buildJSONObject(TestData.validXML);
    }
    
    @Test
    public void BuildValidXML() throws Exception{
        Assert.assertNotNull(ItemFactory.buildXML(TestData.validXML));
    }
    
    @Test(expected = Throwable.class )
    public void BuildInvalidXML() throws Exception{
        ItemFactory.buildXML(TestData.validJSON);

    }
    
    @Test
    public void BuildValidItem() throws Throwable{
        Assert.assertNotNull(ItemFactory.buildItem(TestData.itemName, TestData.validJSON, 1));
        Assert.assertNotNull(ItemFactory.buildItem(TestData.itemName, TestData.validXML, 2));
        
    }
    
    @Test(expected = InvalidItemTypeException.class)
    public void BuildInvalidItemType() throws Throwable{
        ItemFactory.buildItem(TestData.itemName, TestData.validXML, 0);
    }
}
