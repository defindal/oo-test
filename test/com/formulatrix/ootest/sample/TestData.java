/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.sample;

/**
 *
 * @author deph
 */
public class TestData {
    public static final String itemName = "datum";
    public static final String validJSON = "{\"id\":2,\"name\":\"deph\"}";
    public static final String validXML = "<?xml version='1.0' encoding='UTF-8'?><rss></rss>";
    public static final String JSONItemID = "JSON ITEM ID";
    public static final String XMLItemID = "XML ITEM ID";
}
