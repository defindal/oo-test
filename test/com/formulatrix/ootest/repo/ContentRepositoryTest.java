/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.repo;

import com.formulatrix.ootest.entity.Item;
import com.formulatrix.ootest.entity.JSONItem;
import com.formulatrix.ootest.entity.XMLItem;
import com.formulatrix.ootest.sample.TestData;
import junit.framework.Assert;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author deph
 */
public class ContentRepositoryTest {
    
    private static ContentRepository contentRepository;
    private static Item itemToDelete;
    private static Item itemToSearch ;
    
    @BeforeClass
    public static void setUpClass() throws Throwable{
        contentRepository = ContentRepository.getInstance();
        itemToSearch = new JSONItem("SearchMe", TestData.validJSON);
        contentRepository.persist(itemToSearch);
        
        itemToDelete = new XMLItem("DeleteMe", TestData.validXML);
        contentRepository.persist(itemToDelete);
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class ContentRepository.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        ContentRepository result = ContentRepository.getInstance();
        assertNotNull(result);
    }

    /**
     * Test of persist method, of class ContentRepository.
     */
    @Test
    public void testPersist() throws Throwable {
        int initSize = contentRepository.getSize();
        
        Item item1 = new JSONItem("JSONItem", TestData.validJSON);
        contentRepository.persist(item1);
        
        Item item2 = new XMLItem("XMLItem", TestData.validXML);
        contentRepository.persist(item2);
        
        int finalSize = contentRepository.getSize();
        assertEquals(initSize + 2, finalSize);
        
    }

    /**
     * Test of remove method, of class ContentRepository.
     */
    @Test
    public void testRemove() {
        System.out.println("remove");
        
        int initSize = contentRepository.getSize();
        contentRepository.remove(itemToDelete);

        assertEquals(initSize -1, contentRepository.getSize());
    }

    /**
     * Test of findByName method, of class ContentRepository.
     */
    @Test
    public void testFindByName() {
        System.out.println("findByName");
                
        assertEquals(contentRepository.findByName(itemToSearch.getItemName()), itemToSearch);
        
        
    }
    
}
