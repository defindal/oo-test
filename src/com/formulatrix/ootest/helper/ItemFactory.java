/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.helper;

import com.formulatrix.ootest.entity.Item;
import com.formulatrix.ootest.entity.ItemType;
import com.formulatrix.ootest.entity.JSONItem;
import com.formulatrix.ootest.entity.XMLItem;
import com.formulatrix.ootest.exception.InvalidContentException;
import com.formulatrix.ootest.exception.InvalidItemTypeException;
import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author deph
 */
public class ItemFactory {

    public static Item buildItem(String name,String content,int type) throws ParseException, ParserConfigurationException, SAXException, IOException, InvalidItemTypeException, InvalidContentException{
        
        ItemType itemType = ItemType.values()[type];
        switch(itemType){
            case JSON:
                return new JSONItem(name,content);
            case XML:
                return new XMLItem(name, content);
            default:
                throw new InvalidItemTypeException();
        }        
    }
    
    public static JSONObject buildJSONObject(String content) throws ParseException{
        return (JSONObject) new JSONParser().parse(content);       
    }
    
    public static Document buildXML(String content) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilder builder = DocumentBuilderFactory
                    .newInstance()
                    .newDocumentBuilder();
        InputSource source = new InputSource(new StringReader(content));
        return builder.parse(source);
            
         
    }
}
