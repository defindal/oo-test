/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.helper;

import com.formulatrix.ootest.entity.ItemType;
import com.formulatrix.ootest.exception.InvalidItemTypeException;
import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * this class is used to validate content and its type
 * 1 must be JSON
 * 2 must be XML
 * 
 * @author deph
 */
public class ItemValidator {

    public static boolean validate(String content, int type) throws InvalidItemTypeException {

        ItemType itemType = ItemType.values()[type];

        switch (itemType) {
            case JSON:
                return isValidJSON(content);
            case XML:
                return isValidXML(content);
            default:
                throw new InvalidItemTypeException();
        }

    }

    /***
     * Determine whether a string contains valid JSON Object or not
     * @param content the JSON string which will be validated
     * @return true if valid, false otherwise
     */
    public static boolean isValidJSON(String content) {
        try {
            new JSONParser().parse(content);
            return true;
        } catch (ParseException ex) {
            return false;
        }
    }

    /**
     * Determine whether the input is valid XML String
     * @param content the XML String which will be validated
     * @return true if valid , false otherwise
     */
    public static boolean isValidXML(String content) {
        try {
            DocumentBuilder builder = DocumentBuilderFactory
                    .newInstance()
                    .newDocumentBuilder();
            InputSource source = new InputSource(new StringReader(content));
            builder.parse(source);
            return true;
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            return false;
        }
    }
}
