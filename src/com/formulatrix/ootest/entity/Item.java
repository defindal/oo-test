/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.entity;

import com.formulatrix.ootest.exception.NullContentException;
import com.formulatrix.ootest.helper.Message;
import java.io.Serializable;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;

/**
 *
 * @author deph
 * 
 */
public abstract class Item implements Serializable {
    
    protected String itemName;
    protected ItemType itemType;
    
    /**
     * @return the itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * @param itemName the itemName to set
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }  
    
    public abstract String getContent() throws NullContentException;
   

    /**
     * @return the itemType
     */
    public ItemType getItemType() {
        return itemType;
    }

    /**
     * @param itemType the itemType to set
     */
    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }
    
    
}
