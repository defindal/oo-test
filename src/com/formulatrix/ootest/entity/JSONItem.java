/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.entity;

import com.formulatrix.ootest.exception.InvalidContentException;
import com.formulatrix.ootest.exception.InvalidItemTypeException;
import com.formulatrix.ootest.exception.NullContentException;
import com.formulatrix.ootest.helper.ItemFactory;
import com.formulatrix.ootest.helper.ItemValidator;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 *
 * @author deph
 */
public class JSONItem extends Item{

    private JSONObject content;
    
    public JSONItem(String itemName,String itemContent) throws ParseException, InvalidContentException{
        this.itemName = itemName;      
        this.itemType = ItemType.JSON;
        if(ItemValidator.isValidJSON(itemContent)) content = ItemFactory.buildJSONObject(itemContent);
        else throw new InvalidContentException();
    }
    
    @Override
    public String getContent() throws NullContentException {
         if(content != null) return content.toJSONString();
         else throw new NullContentException();
         
    }
    
}
