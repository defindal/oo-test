/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.entity;

import com.formulatrix.ootest.exception.InvalidContentException;
import com.formulatrix.ootest.exception.NullContentException;
import com.formulatrix.ootest.helper.ItemFactory;
import com.formulatrix.ootest.helper.ItemValidator;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author deph
 */
public class XMLItem extends Item{

    private Document content;
    
    public XMLItem(String itemName,String itemContent ) throws InvalidContentException, IOException, ParserConfigurationException, SAXException{
        this.itemName = itemName;      
        this.itemType = ItemType.XML;
        
        if(ItemValidator.isValidXML(itemContent)) content = ItemFactory.buildXML(itemContent);
        else throw new InvalidContentException();
    }
    
    @Override
    public String getContent() throws NullContentException {
         if(content != null) return content.toString();
         else throw new NullContentException();
    }
    
}
