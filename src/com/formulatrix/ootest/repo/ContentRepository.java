/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.repo;

import com.formulatrix.ootest.abstr.IRepository;
import com.formulatrix.ootest.entity.Item;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author deph
 */
public class ContentRepository implements IRepository{

    private static final ContentRepository instance = new ContentRepository();
    private static Map<String,Item> contents;
    
    /**
     * Hide constructor to make sure Singleton Pattern applied
     */
    private ContentRepository(){}
    
    /**
     * the only way to create this object
     * @return instance of this object
     */
    public static ContentRepository getInstance(){ 
        if(contents == null) {
            contents = new HashMap<String,Item>();
        }
        return instance;
    }
    
    @Override
    public void persist(Item item) {
        contents.put(item.getItemName(), item);
    }

    @Override
    public void remove(Item item) {        
        contents.remove(item.getItemName());
    }
    
    
    public Item findByName(String name) {       
        if(contents.containsKey(name)){
            return contents.get(name);
        } else return null;
    }

    @Override
    public int getSize() {
        if(contents == null) return 0;
        return contents.size();
    }

   
    
    
    
    
}
