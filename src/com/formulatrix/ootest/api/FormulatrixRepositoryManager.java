/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.api;

import com.formulatrix.ootest.abstr.IRepositoryManager;
import com.formulatrix.ootest.entity.Item;
import com.formulatrix.ootest.exception.InvalidContentException;
import com.formulatrix.ootest.exception.InvalidItemTypeException;
import com.formulatrix.ootest.exception.NullContentException;
import com.formulatrix.ootest.helper.ItemFactory;
import com.formulatrix.ootest.helper.ItemValidator;
import com.formulatrix.ootest.helper.Message;
import com.formulatrix.ootest.repo.ContentRepository;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;

/**
 *
 * @author deph
 */
public class FormulatrixRepositoryManager implements IRepositoryManager {

    private ContentRepository repository;

    /***
     * Initialize the repository for use
     */
    private void Initialize() {
        if (repository == null) {
            repository = ContentRepository.getInstance();
        }
    }

    /**
     * *
     * Store an item to the repository.
     *
     * @param itemName the ID of item
     * @param itemContent content to be persisted 
     * @param itemType Parameter itemType is used to differentiate JSON or XML.
     * 1 = itemContent is a JSON string. 2 = itemContent is an XML string.
     *
     */
    @Override
    public void Register(String itemName, String itemContent, int itemType) {

        if (repository.findByName(itemName) == null) {
            try {
                ItemValidator.validate(itemContent, itemType);                
                repository.persist(ItemFactory.buildItem(itemName, itemContent, itemType));
            } catch (ParseException | ParserConfigurationException | SAXException | IOException | InvalidItemTypeException | InvalidContentException ex) {
                Logger.getLogger(FormulatrixRepositoryManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Retrieve an item from the repository
     * @param itemName
     * @return the string representation of the item being searched
     */
    @Override
    public String Retrieve(String itemName) {
        Item result;
        if ((result = repository.findByName(itemName)) != null) {
            try {
                return result.getContent();
            } catch (NullContentException ex) {
                return Message.CONTENT_ERROR;
            }
        } else {
            return Message.ITEM_NOT_FOUND;
        }
    }

    /**
     * *
     * Retrieve the type of the item (JSON or XML)
     *
     * @param itemName to item to be searched
     * @return 1 for JSON, 2 for XML , -1 if item not found 
     */
    @Override
    public int GetType(String itemName) {
        Item result;
        if ((result = repository.findByName(itemName)) != null) {
            return result.getItemType().ordinal();
        } else {
            return -1;
        }
    }

    /**
     * Remove an item from the repository.
     * @param itemName the item to be removed
     */
    @Override
    public void Deregister(String itemName) {
        Item result;
        if ((result = repository.findByName(itemName)) != null) {
            repository.remove(result);
        }
    }

}
