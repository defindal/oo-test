/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.abstr;

/**
 *
 * @author deph
 */
public interface IRepositoryManager {
    
    void Register(String itemName, String itemContent,int itemType);
    String Retrieve(String itemName);
    int GetType(String itemName);
    void Deregister(String itemName);
    
    
}
