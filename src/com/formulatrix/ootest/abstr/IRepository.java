/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formulatrix.ootest.abstr;

import com.formulatrix.ootest.entity.Item;

/**
 *
 * @author deph
 */
public interface IRepository {
    
    void persist(Item item);
    void remove(Item item);
    int getSize();
}
